# FS-1030-Group-P

1.  In the mySQL folder, once run "npm install" you first need to run: "node group_Project.js" and then
    run: "node demo_db_connection".

2.  In the node-js folder, once run: "npm install" you need to open POSTMAN =>
    in the POST route: http://localhost:8080/api/auth/signup

        raw-JSON:

            {
                "username": "admin",
                "email": "admin@gmail.com",
                "password": "adminpassword",
                "roles": ["admin"]
            }

            And Again with:

                {
                    "username": "mod",
                    "email": "mod@gmail.com",
                    "password": "modpassword",
                    "roles": ["moderator"]
                }

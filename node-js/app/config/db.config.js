module.exports = {
  HOST: "localhost",
  USER: "nodeclient",
  PASSWORD: "123456",
  DB: "group_Project",
  dialect: "mysql",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};

var mysql = require( 'mysql' );

var con = mysql.createConnection( {
    host: "localhost",
    user: "nodeclient",
    password: "123456"
} );
con.connect( function ( err ) {
    if ( err ) throw err;
    console.log( "Connected!" );
    con.query( "CREATE DATABASE group_Project", function ( err, result ) {
        if ( err ) throw err;
        console.log( "Database created" );
    } );
} );
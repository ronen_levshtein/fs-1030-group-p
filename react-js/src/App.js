import React, { Component } from "react";
import { Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import AuthService from "./services/auth.service";

import Login from "./components/login.component";
import Register from "./components/register.component";
import Profile from "./components/profile.component";
import BoardUser from "./components/board-user.component";
import BoardModerator from "./components/board-moderator.component";
import Doctors from "./components/doctors";
import patientDetails from "./components/patientDetails";
import Search from "./components/searchPatients";
import createNewPatient from "./components/createNewPatient";
import createNewDoctor from "./components/createNewDoctor";

class App extends Component {
  constructor(props) {
    super(props);
    this.logOut = this.logOut.bind(this);

    this.state = {
      showModeratorBoard: false,
      showDoctors: false,
      showPatient: false,
      showSearch: false,
      currentUser: undefined,
    };
  }

  componentDidMount() {
    const user = AuthService.getCurrentUser();

    if (user) {
      this.setState({
        currentUser: user,
        showModeratorBoard: user.roles.includes("ROLE_MODERATOR"),
        showAdminBoard: user.roles.includes("ROLE_ADMIN"),
        showDoctors: user.roles.includes("ROLE_ADMIN"),
        showSearch: user.roles.includes("ROLE_ADMIN","ROLE_MODERATOR"),
        showPatient: user.roles.includes("ROLE_USER"),
        showRegister: user.roles.includes("ROLE_ADMIN"),
      });
    }
  }

  logOut() {
    AuthService.logout();
  }

  render() {
    const { currentUser, showModeratorBoard, showAdminBoard, showPatient } = this.state;

    return (
      <div>
        <nav className="navbar navbar-expand navbar-dark bg-dark">
          <span className="logo">&#9877;&#65039;EMR Mock Up</span> 
          <div className="navbar-nav mr-auto">
            
            {showModeratorBoard && (
              <li className="nav-item">
                <Link to={"/search"} className="nav-link">
                  Patients
                </Link>
              </li>
            )}

            {showAdminBoard && (
              <li className="nav-item">
                <Link to={"/search"} className="nav-link">
                  Patients
                </Link>
              </li>
              
            )}

            {showAdminBoard && (
              <li className="nav-item">
                <Link to={"/doctors"} className="nav-link">
                  Doctors
                </Link>
              </li>
              
            )}

            {showPatient && (
              <li className="nav-item">
                <Link to={"/details"} className="nav-link">
                  Record
                </Link>
              </li>
              
            )}

          </div>

          {currentUser ? (
            <div className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link to={"/profile"} className="nav-link">
                  {currentUser.username}
                </Link>
              </li>
              <li className="nav-item">
                <a href="/" className="nav-link" onClick={this.logOut}>
                  LogOut
                </a>
              </li>
            </div>
          ) : (
            <div className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link to={"/login"} className="nav-link">
                  Login
                </Link>
              </li>

              <li className="nav-item">
                <Link to={"/register"} className="nav-link">
                  Sign Up
                </Link>
              </li>
            </div>
          )}
        </nav>

        {/* <div className="container mt-3"> */}
          <Switch>
            <Route exact path="/" component={Login} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/profile" component={Profile} />
            <Route path="/user" component={BoardUser} />
            <Route path="/mod" component={BoardModerator} />
            <Route path="/doctors" component={Doctors} />
            <Route path="/search" component={Search} />
            <Route path="/details" component={patientDetails} />
            <Route path="/createNewPatient" component={createNewPatient} />
            <Route path="/createNewDoctor" component={createNewDoctor} />
          </Switch>
        {/* </div> */}
      </div>
    );
  }
}

export default App;

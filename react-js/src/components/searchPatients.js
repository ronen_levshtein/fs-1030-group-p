import React, {Component} from 'react'
import MockData from './MOCK_DATA.json'
import {Link, useHistory} from 'react-router-dom'
import AuthService from "../services/auth.service";


class Search extends Component {
    constructor(props) {
        super(props);

        this.state = {
        searchTerm: "",
        setSearchTerm: "",
        showButton: false
        };
    }

    componentDidMount() {
        const user = AuthService.getCurrentUser();

        if (user) {
        this.setState({
            currentUser: user,
            showButton: user.roles.includes("ROLE_ADMIN"),
        });
        }
    }

    // getFilteredPatient () {
    //     this.state.MockData.filter( s => s.first_name.includes(this.state.searchTerm) || s.last_name.includes(this.state.searchTerm) )
    // }

    render() {
    const { showButton, searchTerm } = this.state;
    
    return(
        <div className="containerMain">
           {/*search container */}
            <div className="searchContainer">

            {/*search input */}
                <div className="searchInput">
            <input 
            type="text" 
            placeholder="Patient Search" 
            className="form-control" 
            className="searchBar"
            // onChange = { this.getFilteredPatient().map(s => s.first_name) }
            ></input>
            {showButton && (<button><Link to="/createNewPatient">Create new patient</Link></button>)}
            
            
            </div>

            {/* patient data table */}
            <table className="tableSearch">
                <thead className="thead-dark">
                    <tr>
                        <th className="head1">First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th className="head2">Select</th>
                    </tr>
                </thead>
            <tbody>

                {/* load table data */}
                    {MockData.filter(value=>{
                        if(searchTerm === ''){
                            return value;
                        } else if(
                            value.first_name.includes(value) ||
                            value.last_name.includes(value) ||
                            value.email.includes(value)
                        ){
                            return value
                        }
                        }
                    ).map((m)=> (
                        <tr key={m.id}>
                                <td>{m.first_name}</td>
                                <td>{m.last_name}</td>
                                <td>{m.email}</td>
                                <td><Link to="/details" className="btn btn-primary">Select</Link></td>
                                
                        </tr>
                    ))}
                </tbody>
            </table>
            </div>
        </div>
    );

}
}

export default Search

import React, {useState} from 'react'
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col } from 'reactstrap';
import classnames from 'classnames';


const Details = (props) => {

  
  const [activeTab, setActiveTab] = useState('1');

  const toggle = tab => {
    if(activeTab !== tab) setActiveTab(tab);
  }

  return (
      
    <div className="containerMain">

      {/*details container */}
        <div className="detailDisplay">

       {/*details patient card display */}   
       <h1 className="detailsHeadTitle">
           Patient Details
       </h1>    
       <div className="detailsHeader">
        <p>Patient ID : Fake ID</p>
        <p>Patient Name : Fake Name</p>
        <p>Patient DOB : 1900/01/01</p>
        <p>Gender : F</p>
        <p>Blood Type: A</p>
        <p>Health Card Number : xxxx xxxxx</p>
      </div>

      {/*tabs container */}
      <Nav tabs>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '1' })}
            onClick={() => { toggle('1'); }}
          >
            <strong>Contact Details</strong>
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '2' })}
            onClick={() => { toggle('2'); }}
          >
           <strong>Visits</strong>
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '3' })}
            onClick={() => { toggle('3'); }}
          >
           <strong>Lab Tests</strong>
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '4' })}
            onClick={() => { toggle('4'); }}
          >
           <strong>Medical History</strong>
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '5' })}
            onClick={() => { toggle('5'); }}
          >
           <strong>Billing and Payments</strong>
          </NavLink>
        </NavItem>
      </Nav>

      {/*tabs content */}
      <TabContent activeTab={activeTab}>
        <TabPane tabId="1"> 
          <Row>
                         
              <h4 className="detailsTitle">Details</h4>
              <div className="detailsBox">
                <p>Email: </p>
                <p>Phone: </p>
                <p>Address: </p>
                <p>Postal Code: </p>
                <p>Province: </p>
              </div>             
            
          </Row>
        </TabPane>
        <TabPane tabId="2">
          <Row>
            <h4 className="detailsTitle">Visits</h4>
            <div className="detailsBox">
              <p>Visit 1</p>
              <p>Visit 2</p>
              <p>Visit 3</p>
            </div>
            
          </Row>
        </TabPane>
        <TabPane tabId="3">
          <Row>
            
            <h4 className="detailsTitle">Tests</h4>
            <div className="detailsBox">
              <p>Test 1</p>
              <p>Test 2</p>
              <p>Test 3</p>
            </div>
          </Row>
        </TabPane>
        <TabPane tabId="4">
          <Row>
          
            <h4 className="detailsTitle">History</h4>
            <div className="detailsBox">
              <p>History 1</p>
              <p>History 2</p>
              <p>History 3</p>
            </div>
          </Row>
        </TabPane>
        <TabPane tabId="5">
          <Row>
          <h4 className="detailsTitle">Payments</h4>
            <div className="detailsBox">
              <p>History 1</p>
              <p>History 2</p>
              <p>History 3</p>
            </div>
          </Row>
        </TabPane>
      </TabContent>
      </div>
    </div>
    
  );
}




export default Details

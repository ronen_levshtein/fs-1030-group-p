import React, {useState} from 'react'
import MockData from './MOCK_DATA.json'
import {Link, useHistory} from 'react-router-dom'


const Search = (props) => {

    {/*use state search functionality */}
    const [searchTerm, setsearchTerm] = useState("")

    let history = useHistory();

    const createNewDoctor = () => {
        history.push("/createNewDoctor")
    }

    const getPatientID = (clicked_id) => {

        let selectedID = (clicked_id)
        alert(selectedID)

    }
    
    return(
        <div className="containerMain">
           {/*search container */}
            <div className="searchContainer">

            {/*search input */}
                <div className="searchInput">
            <input 
            type="text" 
            placeholder="Doctor Search" 
            className="form-control" 
            className="searchBar"
            onChange = {(e)=>{
                setsearchTerm(e.target.value);
            }}
            ></input>
            <button type="submit" onClick={createNewDoctor}>Create new doctor</button>
            </div>

            {/* patient data table */}
            <table className="tableSearch">
                <thead className="thead-dark">
                    <tr>
                        <th className="head1">First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th className="head2">Select</th>
                    </tr>
                </thead>
            <tbody>

                {/* load table data */}
                    {MockData.filter(value=>{
                        if(searchTerm === ''){
                            return value;
                        } else if(
                            value.first_name.toLowerCase().includes(searchTerm.toLowerCase()) ||
                            value.last_name.toLowerCase().includes(searchTerm.toLowerCase()) ||
                            value.email.toLowerCase().includes(searchTerm.toLowerCase())
                        ){
                            return value
                        }
                        }
                    ).map((m)=> (
                        <tr key={m.id}>
                                <td>{m.first_name}</td>
                                <td>{m.last_name}</td>
                                <td>{m.email}</td>
                                <td><Link to="/details" className="btn btn-primary">Select</Link></td>
                                
                        </tr>
                    ))}
                </tbody>
            </table>
            </div>
        </div>
    );

}

export default Search
